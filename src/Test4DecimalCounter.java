public class Test4DecimalCounter {
    private int counter;
    private int default_value;
    public Test4DecimalCounter()
    {
        this.counter = this.default_value;
    }
    public Test4DecimalCounter(int value)
    {
        this.counter = value;
    }
    public void increment()
    {
        this.counter++;
    }
    public void decrement()
    {
        this.counter--;
    }
    public void reset()
    {
        this.counter = this.default_value;
    }
    public int getCounter()
    {
        return counter;
    }

    @Override
    public String toString()
    {
        return "DecimalCounter{" + "counter=" + counter + '}';
    }
}

