public class Test2 {
    private double a;
    private double b;
    public Test2(double a, double b)
    {
        this.a = a;
        this.b = b;
    }
    public double getA()
    {
        return a;
    }

    public double getB()
    {
        return b;
    }

    public void setA(double a)
    {
        this.a = a;
    }

    public void setB(double b)
    {
        this.b = b;
    }

    public double getMaximum()
    {
        return Math.max(this.a,this.b);
    }
    @Override
    public String toString()
    {
        return "Test2{" + "a=" + a + ", b=" + b + '}';
    }

}


