public class Test1 {
    private double a;
    private double b;

    @Override
    public String toString()
    {

        return "Test1{" + "a=" + a + ", b=" + b + '}';
    }
    public double getA() {
        return a;
    }

    public void setA(double a)

    {
        if (a < 0) {
            a = 0 ;
        }
        this.a = a;
    }

    public double getMaximum() {
        return Math.max(this.a, this.b);
    }

}
